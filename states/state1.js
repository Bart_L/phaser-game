demo.state1 = function(){};

var cursors;
var vel = 500;
var rocks;
var grass;


demo.state1.prototype = {
    preload: function () {
        //zaladowanie tilemapy - argumenty: nazwa, lokalizacja, null, ostatni argument daje znac ze bedzie to json, bo domyslnie jest CSV
        game.load.tilemap('field', 'assets/tilemaps/field.json', null, Phaser.Tilemap.TILED_JSON);
        //załadowanie poszczególnych elementów mapy - argumenty: nazwa i lokalizacja
        game.load.image('grassTiles', 'assets/tilemaps/grassTiles.png');
        game.load.image('rockTiles', 'assets/tilemaps/rockTiles.png');
        //załadowanie spritea z obrazkiem postaci - argumenty: nazwa oraz lokalizacja pliku
        game.load.image('adam', 'assets/sprites/adam.png');

    },
    create: function () {
        //do zainicjowania obszaru wykluczającego poruszanie się postaci, potrzebny jest silnik Physics ARCADE, musi byc na początku funkcji create
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.backgroundColor = '#DDDDDD';
        addChangeStateEventListeners();

        //dodawanie mapy do gry
        var map = game.add.tilemap('field');
        //dodawanie obrazków do mapy
        map.addTilesetImage('grassTiles');
        map.addTilesetImage('rockTiles');

        //dodawanie warstw do mapy (należy pamiętać o kolejności warstw, aby się nie przysłaniały
        grass = map.createLayer('grass');
        rocks = map.createLayer('rocks');

        //ustawienie kolizji z kamieniami(cyfry z tablicy z pliku json) - argumenty: pierwszy i drugi toindexy obiektów ( w tym wypadku kamienie są od 1 do 9), trzeci to true or false czy kolizje są czy nie, czwarty to warstwa (layer) z obiektami
        map.setCollisionBetween(1,9, true, 'rocks'); //nastepnie trzeba przypisać rocks do fizyki
        //kolizja dla trawy
        map.setCollision(11, true, 'grass');

        //utworzenie postaci na mapie
        adam = game.add.sprite(200, 200, 'adam');
        adam.scale.setTo(0.2, 0.2);

        game.physics.enable(adam);

        //zmienna dla przycisków do skrótowego zapisu
        cursors = game.input.keyboard.createCursorKeys();
    },
    update: function () {
        game.physics.arcade.collide(adam, rocks, function () {
            console.log('hitting rocks!!!');
        });

        game.physics.arcade.collide(adam, grass, function () {
            console.log('hitting grass!!!');
        });

        if(cursors.up.isDown){
            //zamiast używania poniższego zapisu, trzeba użyc velocity dostępnego po zdefiniowaniu fizyki, poniewaz chcemy by adam kolidował z kamieniami
            // adam.y -= speed;
            adam.body.velocity.y = -vel;
         } else if(cursors.down.isDown) {
            adam.body.velocity.y = +vel;
         } else {
            //by nasza postac przestala sie poruszac w danym kierunku gdy klawisz nie jest nacisniety, w przeciwnym wypadku velocity powoduje ze postac leci caly czas w nacisnietą stronę
            adam.body.velocity.y = 0;
         }
        if(cursors.left.isDown) {
            adam.body.velocity.x = -vel;
         } else if(cursors.right.isDown) {
            adam.body.velocity.x = +vel;
         } else {
            //by nasza postac przestala sie poruszac w danym kierunku gdy klawisz nie jest nacisniety, w przeciwnym wypadku velocity powoduje ze postac leci caly czas w nacisnietą stronę
            adam.body.velocity.x = 0;
         }
    }
};