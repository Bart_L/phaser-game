var demo = {};
//zmienna przyjmująca wartość środka planszy na osi X
var centerX = 1500 / 2;
//zmienna przyjmująca wartość środka planszy na osi Y
var centerY = 1000 / 2;
//zdefiniowanie zmiennej dla sprita z postacią
var adam;
//przypisanie zmienniej speed, sluzacej do szybkosci przemieszczania postaci
var speed = 6;

demo.state0 = function(){};
demo.state0.prototype = {
    preload: function () {
        //załadowanie spritea z obrazkiem postaci - argumenty: nazwa oraz lokalizacja pliku
        // game.load.image('adam', 'assets/sprites/adam.png');
        //zamiast obrazka ładujemy spritesheet by postac sie animowała - argumenty: nazwa, lokalizacja, szer, wys
        game.load.spritesheet('adam', 'assets/spritesheets/adamSheet.png', 240, 370);

        //załadowanie spritea z obrazkiem tła - argumenty: nazwa oraz lokalizacja pliku
        game.load.image('tree', 'assets/backgrounds/treeBG.png');
    },
    create: function () {
        //do zainicjowania obszaru wykluczającego poruszanie się postaci, potrzebny jest silnik Physics ARCADE, musi byc na początku funkcji create
        game.physics.startSystem(Phaser.Physics.ARCADE);


        game.stage.backgroundColor = '#DDDDDD';

        //ustawia granice świata - argumenty: granice x i y , oraz szer i wys (w tym wypadku wymiary bkg)
        game.world.setBounds(0,0, 2813, 1000);

        //wywolanie funkcji stanu , podanej ponizej
        addChangeStateEventListeners();

        //skalowanie gry na różnych ekranach - SCALE MANAGER
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        //dodanie wczesniej załadowanego sprita z tłem - argumenty: pozycja x ,pozycja y, nazwa sprita
        var treeBG = game.add.sprite(0, 0, 'tree');

        //dodanie wczesniej załadowanego sprita z postacią - argumenty: pozycja x ,pozycja y, nazwa sprita
        adam = game.add.sprite(centerX, centerY, 'adam');
        //przesunięcie adama w lewo i w górę o 50%, żeby idealnie wyśrodkować
        adam.anchor.x = 0.5;
        adam.anchor.y = 0.5;
        //pomniejszanie postaci o 70 procent - argumenty : wysokość i szerokość (nazwa spritea skala ustaw do)
        adam.scale.setTo(0.7, 0.7);

        //przypisanie physics zainicjowanego wyzej do adama
        game.physics.enable(adam);
        //ustawienie na true kolizj adama z granicami świata
        adam.body.collideWorldBounds = true;

        //inicjalizacja animacji postaci z załadowanych spriteów - argumenty : klucz-nazwa, porządek klatek
        adam.animations.add('walk', [0, 1, 3, 4]);
        //ustawianie kamery na śledzenie adama
        game.camera.follow(adam);
        //deadzone - kwadrat na srodku w ktorym kamera nie sledzi postaci - argumenty: wymiay kwadratu
        game.camera.deadzone = new Phaser.Rectangle(centerX - 300, 0, 600, 1000);

    },
    update: function () {
        //jezeli klawisz PRAWO zostanie wcisniety - wtedy wykonaj akcje
        if(game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
            //pomniejszanie postaci do 70 procent - argumenty : wysokość i szerokość (nazwa spritea skala ustaw do), postać skierowana będzie w oryginalną stronę
            adam.scale.setTo(0.7, 0.7);
            //wtedy adam porusza się po osi x w prawo z prędkością speed
            adam.x += speed;
            //odgrywanie zainicjowanej animacji po wcisnieciu przycisku - argumenty : nazwa animacji, liczba klatek na s, false lub true oznacza zapętlanie bądz nie
            adam.animations.play('walk', 15, true);
        } else if(game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
            //pomniejszanie postaci do 70 procent - argumenty : wysokość i szerokość (nazwa spritea skala ustaw do), postać skierowana będzie w lewą stronę
            adam.scale.setTo(-0.7, 0.7);
            //wtedy adam porusza się po osi x w lewo z prędkością speed
            adam.x -= speed;
            //odgrywanie zainicjowanej animacji po wcisnieciu przycisku - argumenty : nazwa animacji, liczba klatek na s, false lub true oznacza zapętlanie bądz nie
            adam.animations.play('walk', 15, true);
        } else {
            //zatrzymanie animacji, kiedy nie wciskamy powyższych klawiszy, oraz powrót postaci do stanu sprzed animacji
            adam.animations.stop('walk');
            adam.frame = 0;
        }

        if(game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
            //wtedy adam porusza się po osi y w górę z prędkością speed
            adam.y -= speed;
            //jezeli adam przekroczy swoją pozycje na osi y o więcej niz 395px, wtedy jego pozycja ma zostac na poziomie 395px - ma to zapobiec wyskakiwaniu postaci poza drogę
            if(adam.y < 395) {
                adam.y =395;
            }
        } else if(game.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
            //wtedy adam porusza się po osi y w dół z prędkością speed
            adam.y += speed
        }


    }
}

// zmiana stanu - funckja przyjmuje argument i oraz numer stanu, nastepnie statruje stan o numerze przypisanym w argumencie
function changeState(i, stateNum){
    console.log('state' + stateNum);
    game.state.start('state' + stateNum);
}

//funkcja przyjmująca argumenty: klawisz funkcja oraz stan
function addKeyCallback(key, fn, args) {
    game.input.keyboard.addKey(key).onDown.add(fn, null, null ,args);
}

//funckja wywołująca keycallback po nacisnieciu podanych klawiszy
function addChangeStateEventListeners() {
    addKeyCallback(Phaser.Keyboard.ZERO, changeState, 0);
    addKeyCallback(Phaser.Keyboard.ONE, changeState, 1);
    addKeyCallback(Phaser.Keyboard.TWO, changeState, 2);
    addKeyCallback(Phaser.Keyboard.THREE, changeState, 3);
    addKeyCallback(Phaser.Keyboard.FOUR, changeState, 4);
    addKeyCallback(Phaser.Keyboard.FIVE, changeState, 5);
    addKeyCallback(Phaser.Keyboard.SIX, changeState, 6);
    addKeyCallback(Phaser.Keyboard.SEVEN, changeState, 7);
    addKeyCallback(Phaser.Keyboard.EIGHT, changeState, 8);
    addKeyCallback(Phaser.Keyboard.NINE, changeState, 9);
}